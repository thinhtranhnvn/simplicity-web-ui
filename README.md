# simplicity-web-ui

A simple and self-contained Front-End Web Library.

## Folder /src

```
src
├── Accordion    
│   ├── demo.html
│   └── style.css
├── Base
│   ├── demo.html 
│   └── style.css 
├── Button        
│   ├── demo.html 
│   ├── screen.png
│   └── style.css 
├── Carousel     
│   ├── demo.html
│   ├── image
│   │   ├── amitabha-tathagata.jpg      
│   │   ├── avalokitesvara-tathagata.jpg
│   │   ├── bhaisajyaguru-tathagata.jpg 
│   │   └── ratnasambhava-tathagata.jpeg
│   └── style.css
├── Container
│   ├── demo.html
│   └── style.css
├── Dropdown
│   ├── demo.html
│   ├── screen.png
│   └── style.css
├── Figure
│   ├── amitabha-tathagata.jpg
│   ├── demo.html
│   └── style.css
├── Folder
│   ├── demo.html
│   └── style.css
├── Index
│   ├── demo.html
│   ├── screen.png
│   └── style.css
├── Input
│   ├── demo.html
│   ├── screen.png
│   └── style.css
├── Searchbox
│   ├── demo.html
│   ├── screen.png
│   └── style.css
├── main.mjs
└── master.css
```

## Run test

- Install Express: `npm install --save express`
- Run test command: `npm test`
- URL Result pattern: `http://127.0.0.1:9000/{Component}/demo.html`
- The placeholder `{Component}` in URL is a Component's name
- Find Components'names in the naming convention of folders in `/src/`

## Build

The project is not set up for building process. So, to build for production use, you'll have to install tools to bundle source files like Webpack.

All the CSS files are @imported into `/src/master.css`, and all the JS module files are imported into `/src/main.js`. So, you'll only have to point the bundler tools to those two entry points and start the building process.
