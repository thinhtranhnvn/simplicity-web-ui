
const path = require ("path")
const express = require ("express")

const app = express (Infinity)
const pathToSrcFolder = path.join (__dirname, "src")

app.use (express.static (pathToSrcFolder))

app.listen (9000, (_) =>
   [ "Static server started."
   , "Try - http://127.0.0.1:9000/Button/demo.html"
   ] .forEach((msg) => console.log (msg))
) // -- app.listen
